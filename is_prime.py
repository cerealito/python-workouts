"""Write a function that returns true if the given number is prime, false otherwise"""


def is_prime(n: int) -> bool:
    if n <= 1:
        # corner case: one is not a prime
        return False
    # general case: start dividing by 2,3,4.... an so on. If at any point the reminder is 0, this is not a prime
    # upper range is n//2 + 1 because anything bigger will never have a reminder 0
    # (because the result of n//divisor will be smaller than 2)
    for divisor in range(2, n // 2 + 1):
        if n % divisor == 0:
            return False
    return True


def gimme_prime():
    """generator function that yields the next prime number forever, call it to get a generator"""
    n = 2
    while True:
        if is_prime(n):
            yield n
        n += 1


def get_nth_prime(n: int):
    """gets the nth prime number"""
    prime_gen = gimme_prime()  # call the generator function just once to get the generator
    for i in range(2, n + 1):
        next(prime_gen)
    return next(prime_gen)


def get_nth_prime_no_generator(target_index: int):
    """Same as get_nth_prime but without the generator"""
    n = 2
    index_ = 1
    while target_index > 0:  # prevents blocking on wrong input
        if is_prime(n):
            if index_ == target_index:
                break
            else:
                index_ += 1
        n += 1

    return n


if __name__ == '__main__':

    print(get_nth_prime_no_generator)
    for i in range(1, 10):
        print(i, get_nth_prime_no_generator(i))

    print(get_nth_prime)
    for i in range(1, 10):
        print(i, get_nth_prime(i))

