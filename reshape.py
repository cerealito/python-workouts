"""write function reshape which returns all the chars but the spaces in the given string
into a multiline string with lines of size n, for instance
f(3, "abc de fghij") gives:
abc
def
ghi
j
The returned string must not end with \n
"""


def reshape(n, line) -> str:
    line = line.replace(' ', '')
    res = ''
    for i in range(0, len(line), n):
        res += line[i:i+n] + '\n'
    print(res)
    return res[:-1]


if __name__ == '__main__':
    assert "abc\ndef\nghi\nj" == reshape(3, "abc de fghij")
