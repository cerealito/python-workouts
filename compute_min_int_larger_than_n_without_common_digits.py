"""
write a function that receives a positive integer n and returns
the smallest integer that is both:
- larger than n
- has no common digits with n
e.g. f(13) -> 20
"""

def compute_min_larger_than_n_without_common_digits(n):
    res_s = str(1 + int(str(n)[0]))
    for d in str(n)[1:]:
        res_s += '1' if d == '0' else '0'
    return int(res_s)


if __name__ == '__main__':
    n = 654321
    print(n, ':', compute_min_larger_than_n_without_common_digits(n))
    n = 8
    print(n, ':', compute_min_larger_than_n_without_common_digits(n))
    n = 0
    print(n, ':', compute_min_larger_than_n_without_common_digits(n))
    n = 100
    print(n, ':', compute_min_larger_than_n_without_common_digits(n))
    n = 13
    print(n, ':', compute_min_larger_than_n_without_common_digits(n))
