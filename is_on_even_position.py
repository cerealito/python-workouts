"""Write a function that returns true if the given number n is on an even position of the given list"""


def is_on_even_pos(n, lst: list) -> bool:
    return True if lst.index(n) % 2 == 0 else False


if __name__ == '__main__':
    n = 1
    lst = [0, 1, 2]
    print(n, lst, ':', is_on_even_pos(n, lst))

    n = 1
    lst = [0, 0, 1]
    print(n, lst, ':', is_on_even_pos(n, lst))

