"""
sandpile must return the updated pile
- pile is a list of lists [[]] with even length 1, 3, 5, 7...
- n grains will be added at the center
- a cell can hold up to 3 grains
- on the fouth grain, the cell loses its grains to its north, south, east and west neighbours
  (the cell number is back to zero, n,s,e,w neighbours get +1 each)
- if there is no neighbouring cell in a direction, the grain is lost
"""
import math


def pprint(grid):
    for r, row in enumerate(grid):
        print(r, row)


def to_list_of_lists(d: dict):
    lst = int(math.sqrt(len(d)))
    res = []
    for i in range(lst):
        res.append([])
        for j in range(lst):
            res[i].append(d[i, j])
    return res


def step(d):
    for k, v in d.items():
        if v >= 4:
            break
    else:
        return True
    # not stable, modify d
    print("must modify", k, v)
    d[k] -= 4
    try:
        d[k[0] - 1, k[1]] += 1  # North
    except KeyError as e:
        print('grain lost ', e)

    try:
        d[k[0] + 1, k[1]] += 1  # south
    except KeyError as e:
        print('grain lost ', e)

    try:
        d[k[0], k[1] + 1] += 1  # west
    except KeyError as e:
        print('grain lost ', e)

    try:
        d[k[0], k[1] - 1] += 1  # east
    except KeyError as e:
        print('grain lost ', e)

    return False


def new_state_of_sandpile(grid, n):
    d = {}
    for r, row in enumerate(grid):
        for c, value in enumerate(row):
            d[r, c] = value

    print('intial state:')
    pprint(to_list_of_lists(d))

    center = len(grid) // 2
    d[center, center] += n

    stable = False
    i = 1
    while not stable:
        print('====== step', i)
        i += 1
        stable = step(d)
        # pprint(toLists(d))

    return to_list_of_lists(d)


if __name__ == '__main__':
    g = [[0, 0, 3, 0, 0],
         [0, 0, 3, 0, 0],
         [3, 3, 3, 3, 3],
         [0, 0, 3, 0, 0],
         [0, 0, 3, 0, 0]]

    pprint(new_state_of_sandpile(g, 100000))
